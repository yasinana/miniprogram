
/**
 * @function 重写页面page生命周期 先于页面执行
 */
(function expandPage () {
	const originPage = Page
	/**
	 * @function 对Page页面方法重写
	 * @type {Object} config.onLoad
	 * @type {Object} config.onShow
	 * @type {Object} config.onHide
	 * @type {Object} config.onUnload
	 */
	Page = function(config) {
		console.log(config);
		const {onLoad,onShow,onHide,onUnload} = config	
		config.onLoad = function(onLoadOptions){
			if (typeof onLoad === 'function') {
				console.log("onLoad");
				onLoad.call(this,onLoadOptions)
			}
		}
		config.onShow = function(onLoadOptions){
			if (typeof onShow === 'function') {
				console.log("onshow");
				onShow.call(this,onLoadOptions)
			}
		}
		config.onHide = function(onLoadOptions){
			if (typeof onHide === 'function') {
				console.log("onHide");
				onHide.call(this,onLoadOptions)
			}
		}
		config.onUnload = function(onLoadOptions){
			if (typeof onUnload === 'function') {
				console.log("onUnload");
				onUnload.call(this,onLoadOptions)
			}
		}
		
		config.navigateTo = function(url, params) { // 实现一个navigateTo方法，参数包括跳转url和要传递的参数
			this.__params = params
			wx.navigateTo({ url })
		  }
		  
		console.log("最后：",config);
		return originPage(config)
	} 
})()


// (function(){
// 	// 小程序原来的Page方法
// 	let originalPage = Page;
// 	// 我们自定义的Page方法
// 	Page = (config) => {
// 	  // 页面里可以通过this.ajax调用请求接口的方法
// 	  config.ajax = function(){
// 		// 写wx.request的相关代码
// 	  }
// 	  // 默认分享信息
// 	  let defaultShareInfo = {
// 		path: appendInviteId('/pages/index/index'),
// 		title: '全局设置的分享标题'
// 	  }
// 	  // 追加邀请人Id 参数: path(页面路径)
// 	  function appendInviteId(path){
// 		// 分享人Id写成静态的 实际可能要读取缓存
// 		let inviteId = '9998877';
// 		// 路径是否包含inviteId 包含就返回路径
// 		if(path.includes('inviteId')){
// 		  return path;
// 		// 路径不包含inviteId 则追加
// 		} else {
// 		  return path.includes('?') ? `${path}&inviteId=${inviteId}` : `${path}?inviteId=${inviteId}`;
// 		}
// 	  }
// 	  // 重写onShareAppMessage方法
// 	  console.log(config);
// 	  let originalShareMethod = config.onShareAppMessage;
// 	  config.onShareAppMessage = function(e) {
// 		// 配置对象有onShareAppMessage方法
// 		if( originalShareMethod ){
// 		  // 配置对象实际返回的分享信息
// 		  let returnVal =  originalShareMethod.call(this, e)
// 		  // 如果有返回信息
// 		  if(returnVal) {
// 			// 页面的分享信息没有邀请人id 则追加
// 			returnVal.path  = appendInviteId(returnVal.path)
// 			return returnVal
// 		  // 如果页面对象没有返回信息
// 		  } else {
// 			return defaultShareInfo
// 		  }
// 		// 配置对象没有onShareAppMessage方法 直接返回默认的分享信息
// 		} else {
// 		  return defaultShareInfo
// 		}
// 	  }
// 	  // 将配置对象继续想下传递给小程序原来的Page方法
// 	  originalPage (config)
// 	}
//   })();