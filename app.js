import event from "./event.js"
import notification  from "./notification"
import "./pageExtend.js"
App({
  onLaunch() {
    // 把事件挂载在wx上，调用更方便
    wx.event = event
    wx.notification = notification
    // this.extendPage()
  },
  globalData:{
    name:'王二麻子'
  },
  // extendPage(){
  //   const originPage = Page
  //   Page = function(config){
  //     console.log(config);
  //     const {onLoad} = function(onLoadOptions){
  //       onLoad.call(this,onLoadOptions)
  //     }
  //     return originPage(config)
  //   }
  // },
})
