let events = {}

/**
 * @function 注册对象
 * @param {注册名} name 
 * @param {page对象} self 
 * @param {方法} callback 
 */
function on(name,self,callback){
	console.log("on------");
	let tuple = [self,callback];
	let callbacks = events[name]
	console.log(callbacks);
	if (Array.isArray(callbacks)) {
		callbacks.push(tuple)
	}else {
		events[name] = [tuple]
	}
	console.log(events);
}

/**
 * @function 移除通知
 * @param {注册名} name 
 * @param {page对象} self 
 */
function remove(name,self) {
	if (Array.isArray(callbacks)) {
		let callbacks = events[name]
		events[name] = callbacks.filter(tuple=>{
			return tuple[0] != self
		})
	}
}

/**
 * 发送通知
 * @param {注册名} name 
 * @param {参数} data 
 */
function emit (name,data) {
	console.log("name",name,"data:",data);
	let callbacks = events[name]
	console.log(callbacks);
	if (Array.isArray(callbacks)) {
		callbacks.map(tuple=>{
			let self = tuple[0]
			let callback = tuple[1]
			callback.call(self,data)
		})
	}
}

module.exports = {
	on,
	emit,
	remove
}