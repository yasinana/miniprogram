const app = getApp()

Page({
  data: {
    name:app.globalData.name
  },
  onLoad() {
    console.log("子页面数据");
    let param = {
      name: "好嗨哟"
    }
    console.log(this);
  
    
  },
  onGo(){
    this.navigateTo("/second/page",{param:'测试数据'})
  },
  onShow() {
    //注册
    // wx.event.on("tabblist",this,this.init)
    wx.notification.add("TEST_NOTICE",this,this.addNotice)
  },
  onUnload() {
    //移除
    // wx.event.remove("tabblist")
    wx.notification.remove('TEST_NOTICE',this)
  },
  init(e) {
    console.log(e,"this is fake guy");
  },
  addNotice(e) {
    console.log(e,"I am a good guy");
  }
})
