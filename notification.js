/**
 * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
 *               垂直微信小程序开发交流社区
 * github地址: https://github.com/icindy/WxNotificationCenter
 */

/**
 * @type {Array} 储存
 */
let __notice = []

/**
 * @function 注册通知名
 * @param {*} name 注册名
 * @param {*} observe 注册对象，指Page对象
 * @param {*} selector  对应的通知方法，接受到通知后进行的动作
 */
function addNotification(name,observe,selector) {
	let notcie = {
		name: name,
		observe: observe,
		selector:selector,
	}
	addNotices(notcie)
}

function addNotices(newNotice) {
	    if (__notice.length > 0) {
        for (var i = 0; i < __notice.length; i++) {
            var hisNotice = __notice[i];
			//当名称一样时进行对比，如果不是同一个 则放入数组，否则跳出\
			
            if (newNotice.name === hisNotice.name) {
                if (!isObjectChanged(hisNotice, newNotice)) {
                    __notice.push(newNotice);
				}
                return;
            }else{
                __notice.push(newNotice);
            }

        }
    } else {
			__notice.push(newNotice)
    }
	console.log("__notice---------------",__notice);
}

/**
 * @function 判读对象是否相等
 * @param {*} source 
 * @param {*} comparison 
 */
function isObjectChanged(source, comparison) {
 
	// 由于'Object','Array'都属于可遍历的数据类型，所以我们提前定义好判断方法，方便调用
	const iterable = (data) => ['Object', 'Array'].includes(getDataType(data));
  
	// 如果源数据不是可遍历数据，直接抛错，主要用于判断首次传入的值是否符合判断判断标准。
	if (!iterable(source)) {
	  throw new Error(`source should be a Object or Array , but got ${getDataType(source)}`);
	}
  
	// 如果数据类型不一致，说明数据已经发生变化，可以直接return结果
	if (getDataType(source) !== getDataType(comparison)) {
	  return true;
	}
  
	// 提取源数据的所有属性名
	const sourceKeys = Object.keys(source);
  
	// 将对比数据合并到源数据，并提取所有属性名。
	// 在这里进行对象合并，首先是要保证 对比数据>=源数据，好处一：后边遍历的遍历过程就不用做缺省判断了。
	const comparisonKeys = Object.keys({...source, ...comparison});
  
	// 好处二：如果属性数量不一致说明数据必然发生了变化，可以直接return结果
	if (sourceKeys.length !== comparisonKeys.length) {
	  return true;
	}
  
	// 这里遍历使用some，some的特性一旦找到符合条件的值，则会立即return，不会进行无意义的遍历。完美符合我们当前的需求
  
	return comparisonKeys.some(key => {
	  // 如果源数据属于可遍历数据类型，则递归调用
	  if (iterable(source[key])) {
		return isObjectChanged(source[key], comparison[key]);
	  } else {
		return source[key] !== comparison[key];
	  }
	});
  }
  /**
   * @function 判断数据类型
   * @param {any} data 
   */
function getDataType(data) {
	const temp = Object.prototype.toString.call(data);
	const type = temp.match(/\b\w+\b/g);
	return (type.length < 2) ? 'Undefined' : type[1];
  }
  
/**
 * @function 发送通知方法
 * @param {*} name 
 * @param {*} info 
 */
function sendNotification(name,info) {
	console.log(name ,info);

	__notice.forEach(obj=>{
		if (obj.name===name) {
			obj.selector(info)
		}
	})
}	

/**
 * @function 移除通知
 * @param {*} name 
 * @param {*} observe 
 */
function removeNotification(name,observe) {
	for (let i = 0; i < __notice.length; i++) {
		let notice = __notice[i]
		if (notice.name===name && notice.observe===observe) {
			__notice.splice(i,1)
			return 
		}
		
	}
}

module.exports = {
	add : addNotification,
	send : sendNotification,
	remove : removeNotification
}